# KForce Coding Challenge

## Description
This project contains the initial setup and it was created for UI Angular Candidates.

## Features Added
0. Font Awesome 5.8.0
1. Bootstrap 4
2. Google Fonts
3. Angular 8

## Instructions
0. Please create a branch name using the candidate's name.
1. Implement as much angular features as needed.
2. Implement RWD using Flexbox and media queries.

## Evaluation
0. RWD
1. DRY philosophy
2. Code Quality
3. Angular features

## Bonus
Candidates can get some extra bonus if they are able to release more than the minimum requirements, improvements are very welcome.

## Result Expected
You can find the result expected in `https://youtu.be/XefQt8Tg0n8`
